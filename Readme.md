# Automatic 350-1 Tracking

Running the Automatic 350-1 tracking software is very simple and easy. The GUI version works in the same manner as the CLI version. You will need the following installed to use this software:

* tinydb - python based database backend.
* python3 - python 2.x is reaching end of life, program coded in python3.
* tkinter - used for windowing for GUI.
*note: tkinter is only required if you plan to use the GUI.*


## GUI Usage
When the window first initializes, a new database is created in the same folder.
The database is named after your company, change "empire" to whatever your company
is named after before first use.

Upon first initialization, you will see a red warning "No training set!" toward the
top center of the window. The drop down menu to the right of the warning allows you
to set what current training is being taken. Once set, it will change the warning to
the name of the current training being taken.

The center pane provides output from everything done within the program.

The bottom input field allows the user to scan a barcode from the back of the CAC
and automatically sign in that person for the current training.

When a barcode is scanned, if the user does not exist in the users table, a
popup window will appear. In the popup, enter the three character rank, last name, and first name then press the "add" button or hit return. Note that this only adds the user to the
users table and will not sign them in for the current training. To sign them in, simply
re-scan the CAC.

The Export Rosters button is not functional yet, but will be in future releases.
## Todo:
* re-factor code to streamline
* add menu to allow export and remove export button
* add executable for Mac
