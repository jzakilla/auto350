#!/usr/bin/python3

'''Use this to record 350-1 training for your unit and the gen_report.py
function to generate excel compatible .csv files once all training is
complete'''

#imports
from tinydb import TinyDB, Query
from tkinter import *
import time

#Use Tkinter for gui
class Window(Frame):

    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.master = master
        #start main window running
        self.init_window()

    def init_window(self):

        #create tinydb database named after the company using it.
        db = TinyDB('empire.json')
        #create users table
        users = db.table('userlist')
        #create table for current training
        co_training = db.table('current_training')

        #function to output to text pain in middle of main window
        def text_out(input):
            #use insert method to pass input to the end of the current line
            output_window.insert(END, input)
            #use insert to insert new line, moving cursor to next line
            output_window.insert(END, "\n")

        #function to run when an ID card is scanned
        def run_scan(event=None):
            #retrieve value from input box at bottom of main window
            txt = user_input.get()
            #run userCheck function to see if the user is in the users table already
            exists = userCheck(txt)
            #if they don't exist add them
            if exists == False:
                #format string for printing later
                x = f"User with barcode {txt} not found in users database, please add them."
                #send formatted message for output in main window
                text_out(x)
                #call add_user_dialog and pass the barcode value
                add_user_dialog(txt)
            else:
                #If they do exist, update today's training with their data
                update_training(txt)

            #clear the input box at bottom of main window for next entry
            input_spot.delete(0, END)

        def update_training(bar_in):
            #build query object to find record in users table
            User = Query()
            #set the name of the current training
            co_training = db.table(training_input.get())
            #check if user is in training database for current training
            training_taken_already = co_training.contains(User.barcode == bar_in)

            #If they exist in the current training, notify user so they're not added again
            if training_taken_already == True:
                text_out('That user has already taken this training today.')
                return
            #run query against users database using barcode, return result as a list
            #with a length of one. That element is a dictionary with the user's data.
            existing_user = users.search(User.barcode == bar_in)

            #create dict from list
            existing_user = existing_user[0]

            #insert data into table for current class we're taking
            co_training.insert(existing_user)
            #append timedate stamp after that user
            co_training.upsert({'time': time.asctime(time.localtime(time.time()))}, User.barcode == bar_in)
            #notify operator of success
            text_out("User signed in")

        #function to see if user is in users table
        def userCheck(bar_in):
            #build query
            User = Query()
            #run query and store returned value in result variable
            result = users.contains(User.barcode == bar_in)
            #return the result to the calling function
            return result

        #function to add a user when they don't exist in users table
        def add_user(bar_in, r, ln, fn):

            #use insert method to insert user data into users table
            users.insert({'barcode': bar_in, 'rank': r, 'lname': ln, 'fname': fn})
            #create formatted text string
            success_message = f"Successfully added {r} {ln}, {fn} to the user database."
            #print to main output so users can see what was added
            text_out(success_message)

        #create popup window that will allow us to add a user
        def add_user_dialog(bar_in):
            #create main window as toplevel window
            pop = Toplevel()
            #create string variables to hold input values
            rank = StringVar()
            lname = StringVar()
            fname = StringVar()
            #create labels so people know what each entry field is
            rlabel = Label(pop, text="Rank")
            llabel = Label(pop, text="Last Name")
            flabel = Label(pop, text="First Name")
            #Create entry fields for user input
            rentry = Entry(pop, textvariable=rank)
            lentry = Entry(pop, textvariable=lname)
            fentry = Entry(pop, textvariable=fname)
            #create button to submit values. Uses lambda to call add_user function and supplies variables
            sub_button = Button(pop, text="Add User", command=lambda: add_user(bar_in, rank.get(), lname.get(), fname.get()) or pop.destroy())

            #add widgets to window
            rlabel.pack()
            rentry.pack()
            rentry.focus_set()
            llabel.pack()
            lentry.pack()
            flabel.pack()
            fentry.pack()
            #bind enter button to last field so people can hit return and not
            #be forced to click button
            fentry.bind('<Return>', lambda x: add_user(bar_in, rank.get(), lname.get(), fname.get()) or pop.destroy())
            sub_button.pack()

        #function to set the name of the current training
        def training_name(event=None):
            #store current training name in x variable
            x = training_input.get()
            #set current table to the name of the training
            co_training = db.table(x)
            #Set label to message telling what current training is, and colored green
            training_label.config(text=f"Current training is: {training_input.get()}", fg="green")

        self.master.title("Auto 350-1")
        top = Frame(root)
        mid = Frame(root)
        bottom = Frame(root)
        top.pack(side=TOP)
        mid.pack()
        bottom.pack(side=BOTTOM, fill=BOTH, expand=True)

        self.pack(fill=BOTH, expand=1)

        training_set = Button(root, text="Set Training", command=training_name)
        export_button = Button(root, text="Export Rosters", command=add_user_dialog)
        training_label = Label(root, text="No training set!", fg="red")
        training_input = StringVar()
        options = ["TARP", "SHARP", "EO"]

        training_entry = OptionMenu(root, training_input, *(options), command=training_name)

        export_button.pack(in_=top, side=LEFT)
        training_label.pack(in_=top, side=LEFT)
        training_entry.pack(in_=top, side=LEFT)

        #create window for all text output
        output_window = Text(root)
        scrollbar = Scrollbar(root)
        scrollbar.config(command=output_window.yview)
        output_window.config(yscrollcommand=scrollbar.set)
        scrollbar.pack(in_=mid, side=RIGHT, fill=Y)
        output_window.pack(in_=mid, fill=BOTH, expand=True)
        user_input = StringVar()
        input_spot = Entry(root, textvariable=user_input, width=35)
        input_spot.pack(in_=bottom, side=LEFT)
        input_spot.focus_set()
        input_spot.bind('<Return>',run_scan)
        submit = Button(root, text="Submit", command=run_scan)
        submit.pack(in_=bottom, side=LEFT)

root = Tk()
app = Window(root)
root.mainloop()
