#!/usr/bin/python3

#Not sure if I want this to be straight code or class based
#Need tinydb @ tinydb.readthedocs.io

#users table is to store user data.
#all subsequent tables are to store data on current training being taken

#imports
import time
from tinydb import TinyDB, Query
#database to contain the tables, name after unit
db = TinyDB('empire.json')

#table to store user data
users = db.table('userlist')

#Get the name of the current training from the user
CURRENT_CLASS = input('What is the current training: ')
#table to contain classes available
training = db.table(CURRENT_CLASS)

#function to check if in users table
def userCheck(bar_in):
    #build query
    User = Query()
    #run query and store returned value in result variable
    result = users.contains(User.barcode == bar_in)
    #return result
    return result

#function to add user to users table
def add_user(bar_in):
    print('Record not found')
    print('Please type the three character Rank')
    rank = input()
    print('Please type the last name')
    lname = input()
    print('Please type the first name')
    fname = input()
    #insert user information into users table as key: value pairs
    users.insert({'barcode': bar_in, 'rank': rank, 'lname': lname, 'fname': fname})
    #result = users.all()
    #for row in result:
    #    print(row)

def update_training(bar_in):
    #build query to find record in users table
    User = Query()
    #check if user is in training database, if so print error and return
    training_taken_already = training.contains(User.barcode == bar_in)
    if training_taken_already == True:
        print('That user has already taken this training today.')
        return
    #run query against users database using barcode, return result as a list
    #with a length of one. That element is a dictionary.
    existing_user = users.search(User.barcode == bar_in)
    #create dict from list
    existing_user = existing_user[0]
    #insert data into table for current class we're taking
    training.insert(existing_user)
    #append timedate stamp after that user
    training.upsert({'time': time.asctime(time.localtime(time.time()))}, User.barcode == bar_in)


print('You can begin scanning ID cards now, press qq and enter to quit')

while True:
    print("Please scan an ID card: ")
    my_scan = input()
    check_result = userCheck(my_scan)

    if my_scan == 'qq':
        break
    elif check_result == False:
        add_user(my_scan)
        print('User added, please re-scan the ID card to get credit for today\'s training')
    else:
        update_training(my_scan)
