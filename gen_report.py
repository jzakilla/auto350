#!/usr/bin/python3

from tinydb import TinyDB, Query

#get the name of the database file we're working with
db_name = input("What is the name of your company database? ")
#list all tables in database, ask user to select one
db = TinyDB(db_name)

#export contents of selected table as csv
table_list = db.tables()
print(f"{db_name} contains the following rosters: ")
for item in table_list:
    if item == "_default":
        pass
    else:
        print(item)

def file_write(line_in, report_name):
    result = "".join(value + "," for value in line_in.values())
    result = result[:-1]
    result = result + "\n"
    report_name = report_name + ".csv"

    with open(report_name, 'a') as myfile:
        myfile.write(result)

#Ask user what table to export
ex_table = input("Which roster would you like to export? ")
mytable = db.table(ex_table)

#get name of table to export
records = mytable.all()
for item in records:
    file_write(item, ex_table)
#ask if user wants to export another table
